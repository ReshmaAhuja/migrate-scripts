
update
	public.system_config
set
	value = '10,'
	where
	"key" = 'telehealthApptTypeIds';


update
	public.system_config
set
	value = '{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "Self Pay",
			"coverageDescription": "If you don''t have insurance or if you don''t want to use your insurance, you may qualify for a state-funded program or a lower fee scale. If you’re worried about cost, talk to our clinic staff or call 877-855-7526 to ask about how you can get health care that fits your budget. Our fees are based on your household income. We''ll never turn you away from the care you need because of your inability to pay.",
			"check": true
		},
		{
			"id": "2",
			"coverageValue": "Insurance",
			"coverageDescription": "Please Enter the Primary Insurance",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Medicare",
			"coverageDescription": "Test Medicare",
			"check": true
		},
		{
			"id": "4",
			"coverageValue": "Medicaidd",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "5",
			"coverageValue": "Worker''s Comp",
			"coverageDescription": "",
			"check": false
		}
	],
	"showInsuranceForm": [
		"2",
		"3",
		"4"
	],
	"stopBooking": [
		
	]
}'
	where
	"key" = 'INSURANCE_COVERAGE_CONFIG';
	
	
update
	public.system_code_detail
set
	system_code_value_desc = 'Spanish',	
	flag2 = 1
	where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'es';

update
	public.system_code_detail
set
	flag2 = 1,
	is_active = 'Y'
where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'en';
	
	