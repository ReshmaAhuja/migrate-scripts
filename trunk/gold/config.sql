
update
	public.system_config
set
	value = '10,'
	where
	"key" = 'telehealthApptTypeIds';


update
	public.system_config
set
	value = '{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "Self Pay",
			"coverageDescription": "If you don''t have insurance or if you don''t want to use your insurance, you may qualify for a state-funded program or a lower fee scale. If you’re worried about cost, talk to our clinic staff or call 877-855-7526 to ask about how you can get health care that fits your budget. Our fees are based on your household income. We''ll never turn you away from the care you need because of your inability to pay.",
			"check": true
		},
		{
			"id": "2",
			"coverageValue": "Insurance",
			"coverageDescription": "Please Enter the Primary Insurance",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Medicare",
			"coverageDescription": "Test Medicare",
			"check": true
		},
		{
			"id": "4",
			"coverageValue": "Medicaidd",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "5",
			"coverageValue": "Worker''s Comp",
			"coverageDescription": "",
			"check": false
		}
	],
	"showInsuranceForm": [
		"2",
		"3",
		"4"
	],
	"stopBooking": [
		
	]
}'
	where
	"key" = 'INSURANCE_COVERAGE_CONFIG';
	
	
update
	public.system_code_detail
set
	system_code_value_desc = 'Spanish',	
	flag2 = 1
	where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'es';

update
	public.system_code_detail
set
	flag2 = 1,
	is_active = 'Y'
where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'en';
	
UPDATE public.system_code_detail
SET system_code_value_desc='{"name" : "Cancellation Flow"}', text1='{ "config": { "scheduling_flow": "cancel", "is_workflow_disabled": true, "is_reschedule_cancel_panel_visible": true, "is_need_more_options_visible": false, "is_patient_instruction_visible": false, "hide_until_alternate_selected": false, "show_covid_notification": false, "show_earliest_availibility_header": false, "is_map_visible": false, "is_calendar_options_visible": false }, "data": [ { "path": "confirm", "terminal": true } ] }'
WHERE system_code_id='1400' AND system_code_type='30' AND system_code_value='CANCEL' AND eff_end_ts='292278994-08-16 18:00:00.000';

UPDATE public.system_code_detail
SET system_code_value_desc='{"name" : "Confirmation Flow"}', text1='{ "config": { "scheduling_flow": "confirm", "is_reschedule_cancel_panel_visible": false, "is_need_more_options_visible": false, "is_patient_instruction_visible": true, "hide_until_alternate_selected": false, "show_covid_notification": false, "show_earliest_availibility_header": false, "is_map_visible": true, "is_calendar_options_visible": true }, "data": [ { "path": "confirm", "terminal": true } ] }'
WHERE system_code_id='1400' AND system_code_type='30' AND system_code_value='CONFIRM' AND eff_end_ts='292278994-08-16 18:00:00.000';

UPDATE public.system_code_detail
SET system_code_value_desc='{"name": "DASHSelf entire flow"}',text1='{    "config": {        "refferal": false,        "cancellation": false,        "hide_until_alternate_selected": false,        "show_covid_notification": true,        "show_earliest_availibility_header": true     },    "data": [        {            "path": "search",            "title": "SEARCH INFORMATION",            "previous": "Previous",            "next": "Next"        },        {            "path": "patient",            "title": "PATIENT INFORMATION",            "previous": "Previous",            "next": "Next"        },        {            "path": "insurance-option",            "title": "INSURANCE OPTIONS",            "previous": "Previous",            "next": "Next"        },        {            "path": "insurance",            "title": "INSURANCE INFORMATION",            "previous": "Previous",            "next": "Next"        },        {            "path": "details",            "title": "CLINICAL DETAILS",            "previous": "Previous",            "next": "Next"        },        {            "path": "booking",            "title": "BOOK AN APPOINTMENT",            "previous": "Previous",            "next": "Next"        },        {            "path": "demograhics",            "title": "DEMOGRAPHICS",            "previous": "Previous",            "next": "Next"        },        {            "path": "confirm",            "terminal": true        }    ]}'
WHERE system_code_id='1400' AND system_code_type='30' AND system_code_value='DEFAULT' AND eff_end_ts='292278994-08-16 18:00:00.000';

UPDATE public.system_code_detail
SET system_code_value_desc='{"name": "Rescheduling flow"}',text1='{ "config": { "scheduling_flow": "reschedule", "is_workflow_disabled": true, "is_reschedule_cancel_panel_visible": false, "is_need_more_options_visible": false, "is_patient_instruction_visible": true, "hide_until_alternate_selected": false, "show_covid_notification": false, "show_earliest_availibility_header": false }, "data": [ { "path": "details", "title": "CLINICAL DETAILS", "previous": "Previous", "next": "Next" }, { "path": "booking", "title": "BOOK AN APPOINTMENT", "previous": "Previous", "next": "Next" }, { "path": "confirm", "terminal": true } ] }'
WHERE system_code_id='1400' AND system_code_type='30' AND system_code_value='RESCHEDULE' AND eff_end_ts='292278994-08-16 18:00:00.000';

UPDATE public.system_code_detail
SET system_code_value_desc='{"name": "Referral Management Flow"}', text1='{ "config": { "scheduling_flow": "referral", "is_workflow_disabled": true, "is_reschedule_cancel_panel_visible": false, "is_need_more_options_visible": false, "is_patient_instruction_visible": false, "hide_until_alternate_selected": true, "show_covid_notification": false, "show_earliest_availibility_header": false }, "data": [ { "path": "details", "title": "CLINICAL DETAILS", "previous": "Previous", "next": "Next" }, { "path": "booking", "title": "BOOK AN APPOINTMENT", "previous": "Previous", "next": "Next" }, { "path": "confirm", "terminal": true } ] }'
WHERE system_code_id='1400' AND system_code_type='30' AND system_code_value='SCHEDULE' AND eff_end_ts='292278994-08-16 18:00:00.000';
	
	